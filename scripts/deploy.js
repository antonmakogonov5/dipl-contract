// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.

const {ethers, upgrades} = require("hardhat");

async function main() {
    const initialOwner = "0xDd0aAf9FA038015825eaC50D431416C133558eC2";

    const VineCollection = await ethers.getContractFactory("VineCollectionContractV2");
    const instance = await upgrades.deployProxy(VineCollection, [initialOwner], {initializer: 'initialize'});
    await instance.waitForDeployment();
    const address = await instance.getAddress()
    console.log("MyCollectible deployed to:", address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
