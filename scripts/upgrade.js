// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
// const hre = require("hardhat");
const { ethers, upgrades } = require("hardhat");
async function main() {
    // const MyCollectible = await ethers.getContractFactory("VineCollectionContractV2");
    // const initialOwner = "0xDd0aAf9FA038015825eaC50D431416C133558eC2";
    // // const mc = await upgrades.deployProxy(MyCollectible, [initialOwner], { initializer: 'initialize' }  );
    // const VineCollection = await ethers.getContractFactory("VineCollectionContractV2");
    // const instance = await upgrades.deployProxy(VineCollection, [initialOwner], { initializer: 'initialize' }  );
    // await instance.waitForDeployment();
    // // await mc.deployed();
    // console.log("MyCollectible deployed to:", instance.getAddress());
    const BoxV2 = await ethers.getContractFactory("VineCollectionContractV3");
    const upgraded = await upgrades.upgradeProxy("0x3E6b4dae78485d756Ec9F442544cA4238392b25d", BoxV2,
    {
        redeployImplementation:'always',
        kind: 'uups'
    });


}

main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
