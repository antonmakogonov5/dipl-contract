// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "@openzeppelin/contracts-upgradeable/token/ERC1155/ERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/utils/ERC1155HolderUpgradeable.sol";
import "@openzeppelin/contracts/utils/Address.sol";

contract VineCollectionContractV3 is Initializable, ERC1155Upgradeable, OwnableUpgradeable, UUPSUpgradeable, ERC1155HolderUpgradeable {
    using Address for address payable;

    uint256 private currentTokenID;
    uint256 private currentProductCollectionID;
    mapping(uint256 => uint256) public tokenToCollection;
    ProductCollection[] public products;
    CategoryCollection[] public categories;
    mapping(address => WineInStorage) private userStorage;
    uint256 public commissionRate; // Комісія в процентах
    event ProductCollectionCreated(uint256 collectionId);

    struct CategoryCollection {
        uint256 id;
        string name;
    }

    struct ProductCollection {
        uint256 id;
        string name;
        address owner;
        uint256 categoryId;
        uint256 tokenPrice;
        uint256[] tokenIds;
    }

    struct WineInStorage {
        uint256[] tokenIds;
    }

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }

    function initialize(address initialOwner) initializer public {
        __ERC1155_init("");
        __ERC1155Holder_init();
        __Ownable_init(initialOwner);
        __UUPSUpgradeable_init();
        currentTokenID = 0;
        currentProductCollectionID = 0;
        commissionRate = 5;
    }

    function supportsInterface(bytes4 interfaceId) public view override(ERC1155Upgradeable, ERC1155HolderUpgradeable) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    function setURI(string memory newuri) public onlyOwner {
        _setURI(newuri);
    }

    function _authorizeUpgrade(address newImplementation)
    internal
    onlyOwner
    override
    {}

    receive() external payable {}

    // Функція для створення нової категорії
    function createCategory(string memory name) public returns (uint256) {
        uint256 categoryId = categories.length;
        categories.push(CategoryCollection(categoryId, name));
        return categoryId;
    }

    // Функція для редагування існуючої категорії
    function editCategory(uint256 categoryId, string memory newName) public {
        uint index = findCategoryIndexById(categoryId);
        require(index < categories.length, "Category does not exist");

        CategoryCollection storage category = categories[index];
        category.name = newName;
    }

    // Функція для видалення категорії
    function deleteCategoryById(uint256 categoryId) public {
        uint indexToDelete = findCategoryIndexById(categoryId);
        require(indexToDelete < categories.length, "Category does not exist");

        // Перевірка на наявність продуктів
        for (uint i = 0; i < products.length; i++) {
            require(products[i].categoryId != categoryId, "Category has associated products");
        }

        // Видалення категорії
        if (indexToDelete < categories.length - 1) {
            categories[indexToDelete] = categories[categories.length - 1];
        }
        categories.pop();
    }

    function findCategoryIndexById(uint256 categoryId) private view returns (uint) {
        for (uint i = 0; i < categories.length; i++) {
            if (categories[i].id == categoryId) {
                return i;
            }
        }
        revert("Category not found");
    }

    // Функція для отримання всіх категорій
    function getAllCategories() public view returns (CategoryCollection[] memory) {
        return categories;
    }

    function findProductIndexById(uint256 productId) private view returns (uint) {
        for (uint i = 0; i < products.length; i++) {
            if (products[i].id == productId) {
                return i;
            }
        }
        revert("Product not found");
    }

    // Функція для створення нової продуктової колекції
    function createProductCollection(
        string memory name,
        uint256 categoryId,
        uint256 tokenPrice,
        uint256 numberOfTokens
    ) public returns (uint256) {
        uint256 collectionId = currentProductCollectionID++;
        ProductCollection memory newCollection = ProductCollection({
            id: collectionId,
            name: name,
            owner: msg.sender,
            tokenPrice: tokenPrice,
            categoryId: categoryId,
            tokenIds: new uint256[](0)
        });
        products.push(newCollection);
        createMultipleNFTs(collectionId, "", numberOfTokens);
        emit ProductCollectionCreated(collectionId);
        return collectionId;
    }

    function updateProductCollection(
        uint256 productId,
        string memory newName,
        uint256 newTokenPrice,
        uint256 categoryId
    ) public {

        uint indexToDelete = findProductIndexById(productId);
        // Перевірка існування колекції
        require(indexToDelete < products.length, "Collection does not exist");
        // Перевірка, що msg.sender є власником колекції
        require(products[indexToDelete].owner == msg.sender, "Not the owner of the collection");

        // Оновлення властивостей колекції
        ProductCollection storage collectionToUpdate = products[indexToDelete];
        collectionToUpdate.name = newName;
        collectionToUpdate.tokenPrice = newTokenPrice;
        collectionToUpdate.categoryId = categoryId;
    }

    // Delete product collection
    function deleteProductCollectionById(uint256 productId) public {
        uint indexToDelete = findProductIndexById(productId);
        require(indexToDelete < products.length, "Product does not exist");

        if (indexToDelete < products.length - 1) {
            products[indexToDelete] = products[products.length - 1];
        }
        categories.pop();
    }

    function findProductIndexById(uint256 productId) private view returns (uint) {
        for (uint i = 0; i < products.length; i++) {
            if (products[i].id == productId) {
                return i;
            }
        }
        revert("Product not found");
    }

    function createMultipleNFTs(uint256 collectionId, bytes memory data, uint256 numberOfTokens) public returns (uint256) {
        require(collectionId < currentProductCollectionID, "Collection does not exist");
        require(products[collectionId].owner == msg.sender, "Not collection owner");

        for (uint256 i = 0; i < numberOfTokens; i++) {
            uint256 newTokenID = currentTokenID++;
//            _mint(msg.sender, newTokenID, 1, data);
            _mint(address(this), newTokenID, 1, data);
            products[collectionId].tokenIds.push(newTokenID);
            tokenToCollection[newTokenID] = collectionId;
        }

        return collectionId;
    }

    // Отримання інформації про колекцію
    function getCollection(uint256 collectionId) public view returns (ProductCollection memory) {
        return products[collectionId];
    }

    //Buy token directly to customer
    function buyTokens(uint256[] memory collectionIds, uint256[] memory tokenIds) public payable {
        require(collectionIds.length == tokenIds.length, "Mismatched array lengths");

        uint256 totalCost = 0;
        address tokenOwner;
        for (uint256 i = 0; i < collectionIds.length; i++) {
            require(collectionIds[i] < products.length, "Collection does not exist");

            require(tokenIdExistsInCollection(collectionIds[i], tokenIds[i]), "Token does not exist");
            uint productIndex = findProductIndexById(collectionIds[i]);
            uint256 tokenPrice = products[productIndex].tokenPrice;
            tokenOwner = products[productIndex].owner;

            require(tokenOwner != msg.sender, "Owner cannot buy their own token");

            totalCost += tokenPrice;
        }

        require(msg.value >= totalCost, "Insufficient funds for total purchase");

        for (uint256 i = 0; i < collectionIds.length; i++) {
            _safeTransferFrom(address(this), msg.sender, tokenIds[i], 1, "");
            uint productIndex = findProductIndexById(collectionIds[i]);
            removeTokenFromProductCollection(productIndex, tokenIds[i]);
        }

        if (msg.value > totalCost) {
            payable(msg.sender).sendValue(msg.value - totalCost);
        }

        payable(address(this)).sendValue(totalCost);
    }

    function buyAndStoreTokens(uint256[] memory productIds, uint256[] memory amounts) public payable {
        require(productIds.length == amounts.length, "Mismatched arrays");

        uint256 totalCost = 0;
        address tokenOwner;

        for (uint256 i = 0; i < productIds.length; i++) {
            uint productIndex = findProductIndexById(productIds[i]);
            require(productIndex < products.length, "Collection does not exist");

            tokenOwner = products[productIndex].owner;
            uint256 tokenPrice = products[productIndex].tokenPrice;

            totalCost += amounts[i] * tokenPrice;
        }

        require(msg.value >= totalCost, "Insufficient funds for purchase");
        payable(tokenOwner).sendValue(totalCost);

        for (uint256 i = 0; i < productIds.length; i++) {
            uint productIndex = findProductIndexById(productIds[i]);

            for (uint j = 0; j < amounts[i]; j++) {
                uint256 tokenId = products[productIndex].tokenIds[j];
                userStorage[msg.sender].tokenIds.push(tokenId);
                removeTokenFromProductCollection(productIndex, tokenId);
            }

        }

        if (msg.value > totalCost) {
            payable(msg.sender).sendValue(msg.value - totalCost);
        }
    }

    function storeTokens(uint256[] memory tokenIds) public {
        for (uint256 i = 0; i < tokenIds.length; i++) {
            require(balanceOf(msg.sender, tokenIds[i]) > 0, "Token not owned");
            _safeTransferFrom(msg.sender, address(this), tokenIds[i], 1, "");
            userStorage[msg.sender].tokenIds.push(tokenIds[i]);
        }
    }

    function withdrawTokens(uint256[] memory tokenIds) public payable {
        uint256 totalCost = 0;
        for (uint256 i = 0; i < tokenIds.length; i++) {
            require(userOwnsToken(msg.sender, tokenIds[i]), "Token not in storage");
            totalCost += products[tokenIds[i]].tokenPrice;
        }

        uint256 commission = (totalCost * commissionRate) / 100;
        require(msg.value >= commission, "Insufficient commission fee");

        for (uint256 i = 0; i < tokenIds.length; i++) {
            removeTokenFromStorage(msg.sender, tokenIds[i]);
            _safeTransferFrom(address(this), msg.sender, tokenIds[i], 1, "");
        }
    }

    function userOwnsToken(address user, uint256 tokenId) private view returns (bool) {
        WineInStorage storage storageData = userStorage[user];
        for (uint256 i = 0; i < storageData.tokenIds.length; i++) {
            if (storageData.tokenIds[i] == tokenId) {
                return true;
            }
        }
        return false;
    }

    function removeTokenFromStorage(address user, uint256 tokenId) private {
        WineInStorage storage storageData = userStorage[user];
        for (uint256 i = 0; i < storageData.tokenIds.length; i++) {
            if (storageData.tokenIds[i] == tokenId) {
                storageData.tokenIds[i] = storageData.tokenIds[storageData.tokenIds.length - 1];
                storageData.tokenIds.pop();
                break;
            }
        }
    }

    function removeTokenFromProductCollection(uint256 collectionIndex, uint256 tokenId) private {
        uint256[] storage tokenIds = products[collectionIndex].tokenIds;
        for (uint256 i = 0; i < tokenIds.length; i++) {
            if (tokenIds[i] == tokenId) {
                tokenIds[i] = tokenIds[tokenIds.length - 1];
                tokenIds.pop();
                break;
            }
        }
    }

    function tokenIdExistsInCollection(uint256 collectionId, uint256 tokenId) private view returns (bool) {
        uint256[] memory tokens = products[collectionId].tokenIds;
        for (uint256 i = 0; i < tokens.length; i++) {
            if (tokens[i] == tokenId) {
                return true;
            }
        }
        return false;
    }

    function getUserStorage(address user) public view returns (uint256[] memory) {
        return userStorage[user].tokenIds;
    }
}