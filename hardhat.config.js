require('dotenv').config();
// require("@nomiclabs/hardhat-ethers");
// require("@nomiclabs/hardhat-etherscan");

require('@openzeppelin/hardhat-upgrades');

module.exports = {
  defaultNetwork: "polygon_mumbai",
  networks: {
    hardhat: {
    },
    polygon_mumbai: {
      // url: "https://rpc-mumbai.maticvigil.com",
      // url:"https://polygon-mumbai-bor.publicnode.com",
      url:"https://rpc.ankr.com/polygon_mumbai",
      accounts: [process.env.PRIVATE_KEY],
      gas: 21000000,
      gasPrice: 8000000000
    }
  },
  etherscan: {
    apiKey: process.env.POLYGONSCAN_API_KEY
  },
  solidity: {
    version: "0.8.22",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },
};